# Week 1 notes

## Programming in C chapters 1-6

Operating System - controls the entire opteration of a computer.

Compiling - turns source code (human readable) into object code

Linking - links any external object files (including libraries) in with our program's object code to create an executable

First Program:
```C
#include <stdio.h>

int main (void)
{
     printf ("Programming is fun.\n");

     return 0;
}
```

Printing the value of variables:

```C
#include <stdio.h>

int main (void)
{
     int sum;

     sum = 50 + 25;
     printf ("The sum of 50 and 25 is %i\n", sum);

     return 0;
}
```

* `\n` - prints a newline
* can use normal mathematical operators to perform calculations (+, -, *, /, %)
* `%i` - lets you print a numeric value

### Types

* int `%i` - number with no decimal places - `0x` to use hex  `%#x` notation - `0` to use octal notation
* float `%f`, `%e` or `%g` - number with decimal places
* double `%e` or `%g`- float with roughly twice the precision
* char `%c`- single character
* _Bool `%i` or `%u` - 0 or 1 - by convention, 0 is false, 1 is true

**modifiers**

* short `%hi`, `%hx`, `%ho`- smaller int
* long `%li`, `%lx`, `%lo`- bigger int
* long long `%lli`, `%llx`, `%llo`- really bigger int

```C
// Basic conversions in C

#include <stdio.h>

int main (void)
{
      float  f1 = 123.125, f2;
      int    i1, i2 = -150;
      char      c = 'a';

      i1 = f1;                 // floating to integer conversion
      printf ("%f assigned to an int produces %i\n", f1, i1);

      f1 = i2;                 // integer to floating conversion
      printf ("%i assigned to a float produces %f\n", i2, f1);

      f1 = i2 / 100;           // integer divided by integer
      printf ("%i divided by 100 produces %f\n", i2, f1);

      f2 = i2 / 100.0;           // integer divided by a float
      printf ("%i divided by 100.0 produces %f\n", i2, f2);

      f2 = (float) i2 / 100;     // type cast operator
      printf ("(float) %i divided by 100 produces %f\n", i2, f2);

      return 0;
}
/**
123.125000 assigned to an int produces 123
-150 assigned to a float produces -150.000000
-150 divided by 100 produces -1.000000
-150 divided by 100.0 produces -1.500000
(float) -150 divided by 100 produces -1.500000
*/
```